let cwJq = jQuery.noConflict();

cwJq(document).ready(function(){
    cwJq("[cw-data-element=show-more]").click(function() {
        cwJq('.cw-seo-box').addClass("show")
    });
    cwJq("[cw-data-element=show-less]").click(function() {
        cwJq('.cw-seo-box').removeClass("show")
    });
});