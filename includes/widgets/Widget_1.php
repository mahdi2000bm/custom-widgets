<?php namespace CustomWidgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

class Widget_1 extends Widget_Base {

    public function get_name() {
        return 'Accordion_seo';
    }

    public function get_title() {
        return "Accordion Seo";
    }

    public function get_icon() {
        return 'eicon-accordion';
    }

    public function get_categories() {
        return [ 'ihmdone-widgets' ];
    }

    public function get_keywords() {
        return [ 'accordion', 'seo' ];
    }

    protected function register_controls() {

        // Content Tab Start

        $this->start_controls_section(
            'section_title',
            [
                'label' => 'Seo Content',
                'tab' => Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'seoTextArea',
            [
                'label' => "Seo Text Area",
                'type' => Controls_Manager::WYSIWYG,
                'default' => 'Enter seo content.',
            ]
        );

        $this->add_control(
            'seoBoxHeight',
            [
                'label' => "Box Height",
                'type' => Controls_Manager::NUMBER,
                'default' => '50',
                'min' => '25',
                'description' => 'min 25px'
            ]
        );

        // show more title
        $this->add_control(
            'seoMoreTitle',
            [
                'label' => "show more title",
                'type' => Controls_Manager::TEXT,
                'default' => 'بیشتر',
            ]
        );
        // show less title
        $this->add_control(
            'seoLessTitle',
            [
                'label' => "show less title",
                'type' => Controls_Manager::TEXT,
                'default' => 'بستن',
            ]
        );

        $this->end_controls_section();

        // Content Tab End


        // Style Tab Start

        $this->start_controls_section(
            'section_title_style',
            [
                'label' => esc_html__( 'استایل محتوای باکس', 'elementor-addon' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'رنگ متن', 'elementor-addon' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .cw-seo-box-content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'font_family',
            [
                'label' => esc_html__( 'Font Family', 'elementor-addon' ),
                'type' => Controls_Manager::FONT,
                'default' => "'Open Sans', sans-serif",
                'selectors' => [
                    '{{WRAPPER}} .cw-seo-box' => 'font-family: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_section();

        // Style Tab End

    }

    protected function render() {
        $settings = $this->get_settings_for_display(); ?>
        <div class="cw-seo-box">
            <div class="cw-seo-box-content" style="height:<?= $settings['seoBoxHeight']; ?>px">
                <?= $settings['seoTextArea']; ?>
                <div class="cw-overlay"></div>
            </div>
            <button cw-data-element="show-more" class="cw-more">
                <?= $settings['seoMoreTitle']; ?>
            </button>
            <button cw-data-element="show-less" class="cw-less">
                <?= $settings['seoLessTitle']; ?>
            </button>
        </div>
        <?php
    }
}