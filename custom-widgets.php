<?php
/**
 * Plugin Name: Custom Widgets
 * Description: Raw global widgets.
 * Version:     1.0.1
 * Author:      Mehdi Bagheri
 * Author URI:  https://mehdibaqeri.com/
 *
 * Elementor tested up to: 3.16.0
 * Elementor Pro tested up to: 3.16.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

function custom_widgets_addon() {

    define("CW_PLUGIN_URI", plugin_dir_url(__FILE__));

    // Load plugin file
    require_once( __DIR__ . '/includes/Base.php' );

    // Run the plugin
    \CustomWidgets\Base::instance();

}
add_action( 'plugins_loaded', 'custom_widgets_addon' );